# OS イメージの準備と書き込み
以下のいずれかのツールを使用し、Raspberry Pi で使用する microSD に OS のイメージを書き込む。  
基本的には Raspberry Pi Imager で良い。
- [Raspberry Pi Imager](https://www.raspberrypi.org/software/) (Raspberry Pi 公式)
- [Etcher](https://www.balena.io/etcher/)


## フォーマット
1. `CHOOSE OS` を選択  
    ![imager](/img/imager.png)
1. `Erase` を選択  
    ![1](img/format_sd/1.png)
1. `CHOOSE SD CARD` にてドライブを選択
1. `WRITE` を選択
1. `YES` を選択  
    ![2](img/format_sd/2.png)


## 書き込み
1. `CHOOSE OS` を選択  
    ![imager](/img/imager.png)
1. `Other general purpose OS` を選択  
    ![1](/img/select_os/1.png)
1. `Ubuntu` を選択  
    ![2](/img/select_os/2.png)
1. `Ubuntu Server 20.04.1 LTS (RPi 3/4)` を選択  
    ![3](/img/select_os/3.png)
1. `CHOOSE SD CARD` にてドライブを選択
1. `WRITE` を選択


## デバイス類の接続と起動
OS の書き込みが終わった SD カードを Raspberry Pi に挿入、キーボードや HDMI ケーブルも接続。
[初期設定](./UbuntuServer初期設定.md) へ続く
