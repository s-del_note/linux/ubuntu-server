# Ubuntu Server 初期設定


## 準備
- リモート側に SSH のキーペアを作成しておく  
    [SSH Key の作成](https://gitlab.com/s-del_settings/DevelopmentEnvironment/make_ssh-key/-/blob/main/README.md)


## ログイン
1. 初期設定では以下の設定になっているので入力してログイン
    - ユーザー名: `ubuntu`
    - パスワード: `ubuntu`
1. 現在のパスワードと新しいパスワードの入力が求められるので入力  
    デフォルトではキーボードレイアウトが日本語になっていないため、パスワードに記号を入力する場合は注意。


## キーボードレイアウトの変更 (任意)
必要であれば以下の手順で変更する。
1. `sudo dpkg-reconfigure keyboard-configuration`
1. keyboard model:  
    `Generic 105-key (Intl.)`
1. Country of origin for the keyboard:  
    `Japanese`
1. keyboard layout:  
    `Japanese`
1. Key to function as AltGr:  
    `The default for the keyboard layout`
1. Compose key:  
    `No compose key`
1. `# reboot` で再起動


## ネットワークの設定
ubuntu では netplan を使用してネットワーク設定を行う  
`etc/netplan` の中に yaml 形式の設定ファイルを置くことでネットワーク設定を適用できる  
以下は `99-config.yaml` を作成して有線接続の固定 IP 設定を記述した例 (解説記事によくある `50-cloud-init.yaml` は直接編集しない)  

1. ネットワークインターフェース名を `$ ip a` で確認
1. `cd /etc/netplan`
1. `# vim 99-config.yaml`
    - 有線接続の場合
        ```yaml
        network:
          version: 2
          renderer: "networkd"
          ethernets:
            # ネットワークインターフェース名
            eth0:
              # ホームゲートウェイのアドレスを指定
              routes:
                - to: "default"
                  via: "192.168.1.1"
              # DHCP は使用しない
              dhcp4: false
              # プライベートネットワークアドレスを 192.168.1.3 に固定する例
              addresses:
                - "192.168.1.3/24"
              # DNS を変更 (cloudflare を DNS に使用する例)
              nameservers:
                addresses:
                  - "192.168.1.1"
                  - "1.1.1.1"
                  - "1.0.0.1"
                  - "2606:4700:4700::1111"
                  - "2606:4700:4700::1001"
        ```
    - Wi-Fi 接続の場合
        ```yaml
        network:
          version: 2
          renderer: "networkd"
          wifis:
            wlan0:
              dhcp4: false
              access-points:
                # Wi-Fi の SSID を指定する
                "SSID":
                  # Any 接続拒否の場合に true を指定する
                  hidden: true
                  password: "PASSWORD"
              addresses:
                - "192.168.1.3/24"
              nameservers:
                addresses:
                  - "192.168.1.1"
                  - "1.1.1.1"
                  - "1.0.0.1"
                  - "2606:4700:4700::1111"
                  - "2606:4700:4700::1111"
              routes:
                - to: "default"
                  via: "192.168.1.1"
        ```
1. `# netplan apply` するか `# reboot` にて再起動  
    DNS が正しく参照できているかは `$ systemd-resolve --status` で確認できる。  


## デフォルトユーザーアカウント (ubuntu) で SSH 接続 (パスワード認証)
ネットワーク設定が済むと SSH による接続が可能なため、ターミナルや SSH クライアントから接続する。  
画像は RLogin での例
- アドレス: eth0 の addresses に入力したアドレス
- ポート番号: 22
- ユーザー名: ubuntu
- パスワード: 新しく設定した ubuntu のパスワード  
    ![RLogin01](https://gitlab.com/s-del_settings/linux/ubuntu-server/uploads/a07ea21bd0c660b580ffe692b64ebee7/rlogin01.png)


## 新しい管理者アカウントの作成
1. `# useradd -m -s /bin/bash <新しい管理者アカウント名>`
1. `# gpasswd -a <新しい管理者アカウント名> adm admin sudo`
1. `# passwd <新しい管理者アカウント名>`


## SSH の設定変更
許可する接続方法・接続元・ポート番号などを変更する
1. `# ufw allow proto tcp from 192.168.1.0/24 to any port <任意の値>`  
    外部からの接続を許可する場合は `# ufw allow <任意の値>/tcp`
1. `# vim /etc/ssh/sshd_config` にて以下の項目を編集
    ```conf
    # SSH 接続を受け付ける TCP ポート番号を変更
    Port <任意の値>

    # root ユーザーでのログインを無効化
    # 必要に応じて forced-commands-only などに設定
    PermitRootLogin no

    # 利用しないログイン認証方式を無効化
    # パスワード認証
    PasswordAuthentication no
    # チャレンジレスポンス認証
    ChallengeResponseAuthentication no

    # X Window System を利用しないならば無効化
    X11Forwarding no
    ```


## 新たに作成した管理者ユーザーに公開鍵を登録
1. `# su - <作成した管理者アカウント名>` にてユーザー切り替え
1. `$ mkdir .ssh`
1. `$ cd .ssh`
1. `$ vim authorized_keys` で公開鍵を登録


## ufw の有効化と sshd の再読み込み
1. `# systemctl reload sshd`
1. `# ufw enable`  
    `# ufw status` で ufw の状態を確認
1. 一旦接続終了


## 公開鍵認証を利用して SSH 接続
1. 新たに作成した管理者ユーザーでの SSH 接続設定を作成
    - ポート番号: sshd_config に設定したポート番号
    - ユーザー名: 新たに作成した管理者名
    - 認証鍵: 公開鍵とペアの秘密鍵  
        ![rlogin02](https://gitlab.com/s-del_settings/linux/ubuntu-server/uploads/660cfc2725e58229987dde5188e0f686/rlogin02.png)
1. 作成した設定で接続


## デフォルトユーザーアカウント (ubuntu) の削除
新たな管理者アカウントが作成され、デフォルトの ubuntu アカウントは不要となる。  
`# userdel -r ubuntu` で ubuntu アカウントを削除。  
消さずに残しておく場合は `# passwd -d ubuntu` でパスワードを削除しておくか、`# passwd -l ubuntu` でロックしておく。


## apt パッケージの更新
1. `# apt update` で更新可能なパッケージ一覧を取得  
    `$ apt list --upgradable` で取得した一覧を表示できる
1. `# apt upgrade` でパッケージを更新
1. `# apt autoremove` で不要になったパッケージを削除 (このコマンドが必要な場合はメッセージが表示される)

更新が終了したら `# shutdown -h now` でシャットダウン。  
SSH でのログイン設定も完了したため、不要になったキーボードや HDMI ケーブル等の不要なデバイスを取り外せる。  
後は必要に応じて [その他の設定](./その他の設定.md) を行う。


## 参考
- [Raspberry Pi 4にUbuntu Serverを入れて初期設定をするまで【簡単なセキュリティを添えて】 - Qiita](https://qiita.com/quailDegu/items/63114ba1e14416df8040)
- [【Ubuntu】 /etc/netplan/50-cloud-init.yamlを編集するの止めろ - Qiita](https://qiita.com/yas-nyan/items/9033fb1d1037dcf9dba5)
